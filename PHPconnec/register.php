<?php
require 'recaptchalib.php';
  if (isset($_POST['username'], $_POST['password'], $_POST['email'])) {
      require '../php/config.php';
      ///condition telephone

      if (empty($_POST['telephone'])) {
          $teleph = 0;
      } else {
          $teleph = $_POST['telephone'];
      }

      $usr = $_POST['username'];
      $pass = hash('sha256', $_POST['password']);
      $email = $_POST['email'];

      //check le email dans la db
      $sql = dbConnect()->prepare("SELECT * FROM users WHERE email=?");
      $sql->bindParam(1, $_POST["email"]);
      $sql->execute();
      $exists=$sql->rowCount();
      if ($exists) {
          echo "<p class='text-danger'>l'Email est deja pris !";
      }

      //si non je l insert dans la db!
      else {
          $check = dbConnect()->prepare("SELECT * FROM users WHERE password=:password AND email=:email AND username=:username AND telephone=:telephone");

          $check->bindParam(':username', $usr);
          $check->bindParam(':password', $pass);
          $check->bindParam(':email', $email);
          $check->bindParam(':telephone', $teleph);
          $check->execute();

          $row = $check->fetch();
          if ($row['username'] == $usr) {
              echo "<h5>Utilisateur déja present !</h5></br>";
          }
          if ($row['email'] == $email) {
              echo " <h5>Email est deja pris !</h5></br>";
          } else {
              //l insert !:
              $query = dbConnect()->prepare("INSERT INTO `users`(`username`, `password`, `email`, `telephone`, `signup_date`) VALUES ('$usr', '$pass', '$email', '$teleph', '".date("Y-m-d H:i:s")."')");
              $query->execute();

              $_SESSION['username'] = $usr;
              $_SESSION['email'] = $email;
              header("Location: ../index.php?message=Success");
          }
      }
  }
?>
<!DOCTYPE html>
<!--MAIN Page !-->
<html lang="fr">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<!-- Metas Page details-->
<title>Sky-&-Wave</title>
<meta name="description" content="sky-and-wave">
<meta name="author" content="Julioo">
<!-- Mobile Specific Metas-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--main style-->
<link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap.css">
<link rel="stylesheet" type="text/css" media="screen" href="../css/main.css">
<link rel="stylesheet" type="text/css" media="screen" href="../css/responsive.css">
<!--google font style-->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- pictos !-->
<link rel="stylesheet" href="css/font-awesome.css">
<script src="https://www.google.com/recaptcha/api.js"></script>
</head>
  <body class="backgroundHead">
  
  <?php
$reCaptcha = new ReCaptcha("6LdrdEwUAAAAAMtaYy-WgLlwpE_29gyIj5_hwvt7");
if(isset($_POST["g-recaptcha-response"])) {
    $resp = $reCaptcha->verifyResponse(
        $_SERVER["REMOTE_ADDR"],
        $_POST["g-recaptcha-response"]
        );
    if ($resp != null && $resp->success) {echo "OK";}
    else {echo "CAPTCHA incorrect";}
    }
?> 
    <div align="center">
      <h4 ><span>Bienvenue,</span></br>sur</h4>
  <img alt="sky-and-wave" id="titre_main" src="../images/titre_sky.svg">
  <img alt="sky-and-wave" id="titre_2" src="../images/titre_sky.svg">


    <form class="form-group" action="" method="post" style="width:28rem;max-width:58vw;">

<div class="alert alert-success" role="alert">
  <h4 class="alert-heading">Afin de pouvoir accéder au fonctionalitées du site</h4>
  <p>vous devais vous enregistré ici !</p>
</div>


<label for="exampleInputPassword1">Identifiant <span class="text-danger">*</span></label>
<input type="text" name='username' class="form-control" id="exampleInputPassword1" placeholder="Identifiant">



    <label for="exampleInputPassword1">Mot de passe <span class="text-danger">*</span></label>
    <input type="password" name='password' class="form-control" id="exampleInputPassword1" placeholder="Password">



    <label for="exampleInputEmail1">Email <span class="text-danger">*</span></label><small id="emailHelp" class="form-text text-muted">enter un email valide !</small>
    <input type="email"  name='email' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="saisir votre email">
    </br>


    <label for="exampleInputPhone1">Telephone </label>  <small  class="form-text text-muted">facultatif !</small>
    <input type="tel"  name='telephone' class="form-control" placeholder="Numero de Tel.">

</br>
       
        <div class="g-recaptcha" data-sitekey="6LdrdEwUAAAAAKbk58j7eCmlmEBtGgNu-ytDnyT6"></div>
      </br>
            <input type='submit' value="S'inscrire" name="btn1" class="btn btn-success"/>
            <input type="button" onclick="window.location = '../index.php';" value="Retour" class="btn btn-warning">


    </form>
      </div>
  </body>
</html>
