<?php
include('../php/config.php');
  if (isset($_POST['password'], $_POST['email'])) {
      $pass = hash('sha256', $_POST['password']);
      $usr = $_POST['username'];
      $email = $_POST['email'];

      $query = dbConnect()->prepare("SELECT * FROM users WHERE password=:password AND email=:email");
      $query->bindParam(':password', $pass);
      $query->bindParam(':email', $email);

      $query->execute();
      $row = $query->fetch();

      // if($usr == 'BANNED'){
      //   header("Location: ../index.php");
      // }

      if ($row['username'] == $usr and $row['password'] == $pass and $row['email'] == $email) {
          $_SESSION['username'] = $row['username'];
          $_SESSION['email'] = $row['email'];
          $_SESSION['id'] = $row['id'];
          header("Location: ../index.php");
      } else {
          ?>
        <!-- l anim de l'alert -->

  <div id="moitie">
  <div id="div_alert" class="alert alert-danger" role="alert">
  <p class="alert-heading"><button type="button"  onclick="javascript:display('div_alert');" class="close" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>Probleme données incorrect !</p>
  </div>
  </div>
          <?php
      }
  }
?>


<!DOCTYPE html>
<!--MAIN Page !-->
<script>
function display(f)
{
var targetElement;
targetElement = document.getElementById(f) ;
if (targetElement.style.display == "none")
{
targetElement.style.display = "" ;
} else {
document.getElementById("div_alert").style.transform = "scale(0)";
}}
</script>
<html lang="fr">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<!-- Metas Page details-->
<title>Sky-&-Wave</title>
<meta name="description" content="sky&wind">
<meta name="author" content="Julioo">
<!-- Mobile Specific Metas-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--main style-->
<link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap.css">
<link rel="stylesheet" type="text/css" media="screen" href="../css/main.css">
<link rel="stylesheet" type="text/css" media="screen" href="../css/responsive.css">
<!--google font style-->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- pictos !-->
<link rel="stylesheet" href="css/font-awesome.css">
</head>
  <body style="height:100vh;"class="backgroundHead">
      <br>
      <br>
      <div align="center">
    <img id="titre_main" src="../images/titre_sky.svg">
      <img alt="sky-and-wave" id="titre_2" src="../images/titre_sky.svg">
      <form action="" method="post" style="width:28rem;max-width:58vw;">
    <div class="alert alert-info" role="alert">
      <h4 class="alert-heading">si vous êtes déja inscrit sur le site</h4>
      <p class="text-success">renseigner vos Identifiants ici : </p>
    </div>



        <div class="form-group">
  <label for="exampleInputPassword1">Identifiant<span class="text-danger">*</span></label>
  <input type="text" name='username' class="form-control" id="exampleInputPassword1" placeholder="Identifiant">
  </div>

              <div class="form-group">
      <label for="exampleInputPassword1">Mot de passe<span class="text-danger">*</span></label>
      <input type="password" name='password' class="form-control" id="exampleInputPassword1" placeholder="Password">
    </div>

              <div class="form-group">
      <label for="exampleInputEmail1">Email<span class="text-danger">*</span></label>
      <input type="email"  name='email' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="saisir votre email">
      <small id="emailHelp" class="form-text text-muted">enter votre email !</small>
    </div>
              <input type='submit' value='Connection!' name="btn1" class="btn btn-success"/>
              <input type="button" onclick="window.location = '../index.php';" value="Retour" class="btn btn-warning">

      </form>


    </div>
  </body>
</html>
