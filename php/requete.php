
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<!-- Metas Page details-->
<title>Sky-&-Wave</title>
<meta name="description" content="sky&wind">
<meta name="author" content="Julioo">
<!-- Mobile Specific Metas-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--main style-->
<link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap.css">
<link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap-social-gh-pages/bootstrap-social.css">
<link rel="stylesheet" type="text/css" media="screen" href="../css/main.css">
<link rel="stylesheet" type="text/css" media="screen" href="../css/responsive.css">
  <script type="text/javascript" src="../css/bootstrap.js"></script>
<!--google font style-->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- pictos !-->
<link rel="stylesheet" href="../css/font-awesome.css">
</head>
<body style="background: linear-gradient(to left, #fff, #17A2B8);" onUnload="GUnload()">

    	<div id="userHeader">
<?php include('session_user.php');?>
<?php
//petit fix pour id_users google
$reponse = $bdd->query('
SELECT
*
FROM
users
WHERE
username = "'.$_SESSION['username'].'";
');
$donnees = $reponse->fetch();
$_SESSION['id'] = $donnees['id'];
$_SESSION['email'] = $donnees['email'];
///fin du fix///
?>
  <div align="center">
        <div id="div_alert" class="alert alert-light fontold" role="alert">
          <h3 class="fontSeoSc alert-heading border backgroundb text-primary">Voici la liste des MP </h3>
        </div>
        <div id="div_alert" class="alert alert-light text-primary" role="alert" style="max-width:90vw; width:28rem;">
        <p>Les Personnes qui sont interessé par l'une de vos annonces, et qui on souhaité vous répondre et communiqué leur données !</p>
        </div>



  <h4 class="text-light fontold">Réponse :</h4>
<?php
//jointure entre table users et mise_en_relation
$q = 'SELECT * FROM mise_en_relation
INNER JOIN users ON mise_en_relation.id_demandeur = users.id
WHERE
id_receveur = "'.$_SESSION["id"].'"
';


$req = $bdd->query($q);

//afficher dans un while la liste des requetes !
while ($dnn = $req->fetch()) {
    $username = htmlspecialchars($dnn['username']);
    $id = htmlspecialchars($dnn['idreq']);
    //echo $id;
    //echo $username;

    ///Formulaire pour gerer les réponses !?>


<?php //echo $id;?>
<form class="form-group" action="cible_requete.php" method="post">
<input type="hidden" value="<?php echo $id ?>" name="hidden">
<div class="card" style="width: 18rem;">
  <div class="backgroundb card-body">


     <?php echo '<p class="card-text"><strong>Pour votre Annonce :</strong> <a href="zoom.php?id_saisi_offre='.htmlspecialchars($dnn['id_saisi_offre']).'" target="_blank"><span class="text-primary">'.htmlspecialchars($dnn['id_saisi_offre']).'</span></a></p>'; ?>
      <?php echo '<p class="card-text"><span class="text-info"><strong></p>de :'.$username?></strong></span>
      <?php echo '<p class="card-text"><strong>Message : </strong><span class="messageZoom text-primary"> '.htmlspecialchars($dnn['message_requete']).'</p>'?>



        <?php

        //de base date accept = NULL
        if (!empty($dnn['date_acceptation'])) {
            echo  '<strong>Son mail : </strong><span class="text-success">'.htmlspecialchars($dnn['email']).'</span></br>';
            echo '<strong>Son Téléphone : </strong><span class="text-success">'.htmlspecialchars(($dnn['telephone'] == 0 ? 'pas renseigner !' : $dnn['telephone'])) .'</span></br>';
            echo '<strong>Vous avez accepter le: </strong>'.htmlspecialchars($dnn['date_acceptation']);
            echo''; ?>

            <div class="btn-group btn-group-toggle" data-toggle="buttons">
              <label class="btn btn-outline-danger">
                <input type="radio" name="reqSelec" value="Supprimer ?"> Suppr ?
              </label>

</div>
            </form>
            <input type="submit" value="valider" class="btn btn-outline-info"></div></div>
<?php
        } else {
            echo 'à: '.htmlspecialchars($dnn['date_creation']); ?>

            <div class="btn-group btn-group-toggle" data-toggle="buttons">
              <label class="btn btn-outline-danger">
                <input type="radio" name="reqSelec" value="Supprimer ?"> Suppr ?
              </label>
              <label class="btn btn-outline-success">
                <input type="radio" name="reqSelec" value="Accepter ?" >accept ?
              </label>
            </div>
            </form>
          <input type="submit" value="valider" class="btn btn-outline-info"></div></div>
      <?php
        }
}

?>



  <input type="button" onclick="window.location = 'menu.php';" value="Retour &agrave; l'Espace Membre" class="btn btn-secondary">
<hr>
</div>
</div>
</div>
  </body>

  <!-- pour les boutton radios -->
  <!--jquary min js-->
  <script type="text/javascript" src="../js/jquery.min.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <!--for portfolio jquery-->
  <script src="../js/jquery.isotope.js" type="text/javascript"></script>
  <!-- <link type="text/css" rel="stylesheet" id="theme" href="../css/jquery-ui-1.8.16.custom.css"> -->

  <!--about jquery-->
  <script src="../js/jquery.classyloader.min.js"></script>
  <script src="../js/custom.js"></script>
</html>
