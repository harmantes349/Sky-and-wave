
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Project Planche</title>
<meta name="description" content="Offre">
<meta name="author" content="Julioo">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap.css">
<link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap-social-gh-pages/bootstrap-social.css">
<link rel="stylesheet" type="text/css" media="screen" href="../css/font-awesome.css">
<link rel="stylesheet" type="text/css" media="screen" href="../css/main.css">
<link rel="stylesheet" type="text/css" media="screen" href="../css/responsive.css">
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/font-awesome.css">
<script type="text/javascript" src="../css/bootstrap.js"></script>
<script src="https://www.google.com/recaptcha/api.js"></script>
</head>

<body class="backgroundHead" style="overflow-x: hidden;">

<?php 
require 'recaptchalib.php';
include('session_user.php');?>
<div align="center">
  <h2 class="fontSeoSc red text-primary"><span class="fa fa-edit"></span></br>Publie une annonce !</h2>

 <?php
$reCaptcha = new ReCaptcha("6LdrdEwUAAAAAMtaYy-WgLlwpE_29gyIj5_hwvt7");
if(isset($_POST["g-recaptcha-response"])) {
    $resp = $reCaptcha->verifyResponse(
        $_SERVER["REMOTE_ADDR"],
        $_POST["g-recaptcha-response"]
        );
    if ($resp != null && $resp->success) {echo "OK";}
    else {echo "CAPTCHA incorrect";}
    }
?> 

  <form action="cible_saisi_offre.php" method="post" enctype="multipart/form-data" style="max-width:500px;">

<div class="btn bg-info border input-group">
<label class="text-light"  for="exampleInputEmail1"><strong>Titre : </strong></label>
<input class="form-control" id="exampleFormControlInput1" placeholder=" Ici un Titre 25 charac. max!*" type="text" name="titre"  rows="10" cols="50"></input>
</div>
</br>

<div class="btn bg-info border">
<p><strong class="text-light">Une Description : </strong></p> <textarea class="form-control" rows="5" placeholder="255 charac. max!*" type="text" name="message"  rows="10" cols="50"></textarea><br>
</div>


<div class="form-group bg-info border">
<p><strong class="text-light">Renseigner un prix: </strong></p>
<input style="width: 86%;" class="form-control" placeholder="15 charac. max!" name="prix" rows="1" cols="4" type="text"><br>
<span data-bind="text: optionsValue"></span>
</div>




<div class="btn bg-info input-group border">
<p><strong class="text-light">Mon coin:* </strong></p>
<div class="btn-group btn-group-toggle" data-toggle="buttons" style="flex-wrap:wrap;">
  <label  class="btn btn-danger active">
    <input type="radio" name="localisation" value="Sud" checked="true"/>coté Méditerranée
  </label>

  <label class="btn btn-primary">
    <input type="radio" name="localisation" value="Nord" />coté Atlantique
  </label>

  <label class="btn btn-warning">
    <input type="radio" name="localisation" value="pacifique"/>coté pacifique
  </label>
</div>
</div>
<div class="form-group bg-info border">
<p><strong class="text-light">Renseigner un lieu plus précis : </strong></p>
<input style="width: 86%;" class="form-control" placeholder="ecrire le lieu de l'offre*" type="text" name="lieu"  rows="1" cols="4"/><br>
<span data-bind="text: optionsValue"></span>
</div>


<div class="btn bg-info border form-group" style="overflow-x: hidden;">
      <p><strong>Envoi d' Images :</strong>
        <sm class="text-light">(facultatif)</br>les noms d'images portants des caractaires</br>speciaux ne son pas supporter !<sm>
<div class="form-group-file">
    <input type="file" name="monfichier" class="btn btn-primary" /><br />
  </div>
     </p>
   </div>
   <script>
    function onSubmit(token) {
      document.getElementById("demo-form").submit();
    }
  </script>
  

     <div class="g-recaptcha" data-sitekey="6LdrdEwUAAAAAKbk58j7eCmlmEBtGgNu-ytDnyT6"></div>

 <input type="submit" value="Ajouter l'Offre" class="btn btn-success">
<input onclick="window.location = '../index.php';" value="Retour" class="btn btn-secondary">
</form>


  </div>


</body>
<!--jquary min js-->
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/bootstrap.js"></script>
<!--for portfolio jquery-->
<script src="../js/jquery.isotope.js" type="text/javascript"></script>
<!-- <link type="text/css" rel="stylesheet" id="theme" href="../css/jquery-ui-1.8.16.custom.css"> -->

<!--about jquery-->
<script src="../js/jquery.classyloader.min.js"></script>
<script src="../js/custom.js"></script>
</html>
