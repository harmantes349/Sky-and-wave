<?php
include('config.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <link href="../css/main.css" rel="stylesheet" title="Style" />
        <title>Modifier ses informations personnelles</title>
    </head>
    <body class="backgroundHead">
    	<div class="header">
        	<h1><span>E</span>space <span>M</span>embre</h1>
<h5>INPROGRESS</h5>

              <?php if (isset($_SESSION['username'])) {
    echo '<h2 > '.htmlentities($_SESSION['username'], ENT_QUOTES, 'UTF-8').'</h2>';
} ?>

	    </div>
      <a href="<?php echo $url_home; ?>"><h3 class="connexition">Retour &agrave; l'Espace Membre</h3></a>
<?php
//On verifie si lutilisateur est connecte
if (isset($_SESSION['username'])) {
    //On verifie si le formulaire a ete envoye
    if (isset($_POST['username'], $_POST['password'], $_POST['passverif'], $_POST['email'])) {
        //On enleve lechappement si get_magic_quotes_gpc est active
        if (get_magic_quotes_gpc()) {
            $_POST['username'] = stripslashes($_POST['username']);
            $_POST['password'] = stripslashes($_POST['password']);
            $_POST['passverif'] = stripslashes($_POST['passverif']);
            $_POST['email'] = stripslashes($_POST['email']);
            //$_POST['avatar'] = stripslashes($_POST['avatar']);
        }
        //On verifie si le mot de passe et celui de la verification sont identiques
        if ($_POST['password']==$_POST['passverif']) {
            //On verifie si le mot de passe a 6 caracteres ou plus
            if (strlen($_POST['password'])>=6) {
                //On verifie si lemail est valide
                if (preg_match('#^(([a-z0-9!\#$%&\\\'*+/=?^_`{|}~-]+\.?)*[a-z0-9!\#$%&\\\'*+/=?^_`{|}~-]+)@(([a-z0-9-_]+\.?)*[a-z0-9-_]+)\.[a-z]{2,}$#i', $_POST['email'])) {
                    //On echape les variables pour pouvoir les mettre dans une requette SQL
                    $username = htmlspecialchars($_POST['username']);
                    $password = htmlspecialchars($_POST['password']);
                    $email = htmlspecialchars($_POST['email']);
                    //$avatar = htmlspecialchars($_POST['avatar']);
                    //On verifie sil ny a pas deja un utilisateur inscrit avec le pseudo choisis
                    $dnn = $bdd->query('SELECT count(*) AS nb FROM users WHERE username="'.$username.'"');
                    $dn = $dnn->fetch();
                    //On verifie si le pseudo a ete modifie pour un autre et que celui-ci n'est pas deja utilise
                    if ($dn['nb']==0 or $_POST['username']==$_SESSION['username']) {
                        //On modifie les informations de lutilisateur avec les nouvelles
                        if ($bdd->query('UPDATE users SET username="'.$username.'", password="'.$password.'", email="'.$email.'" where id='.$bdd->quote($_SESSION['id_users']))) {
                            //Si ca a fonctionne, on naffiche pas le formulaire
                            $form = false;
                            //On supprime les sessions username et userid au cas ou il aurait modifie son pseudo
                            unset($_SESSION['username'], $_SESSION['id_users']); ?>
<div class="message">Vos informations ont bien &eacute;t&eacute; modifif&eacute;e. Vous devez vous reconnecter.<br />
<a href="connexion.php">Se connecter</a></div>
<?php
                        } else {
                            //Sinon on dit quil y a eu une erreur
                            $form = true;
                            $message = 'Une erreur est survenue lors des modifications.';
                        }
                    } else {
                        //Sinon, on dit que le pseudo voulu est deja pris
                        $form = true;
                        $message = 'Un autre utilisateur utilise d&eacute;j&agrave; le nom d\'utilisateur que vous d&eacute;sirez utiliser.';
                    }
                } else {
                    //Sinon, on dit que lemail nest pas valide
                    $form = true;
                    $message = 'L\'email que vous avez entr&eacute; n\'est pas valide.';
                }
            } else {
                //Sinon, on dit que le mot de passe nest pas assez long
                $form = true;
                $message = 'Le mot de passe que vous avez entr&eacute; contien moins de 6 caract&egrave;res.';
            }
        } else {
            //Sinon, on dit que les mots de passes ne sont pas identiques
            $form = true;
            $message = 'Les mot de passe que vous avez entr&eacute; ne sont pas identiques.';
        }
    } else {
        $form = true;
    }
    if ($form) {
        //On affiche un message sil y a lieu
        if (isset($message)) {
            echo '<strong>'.$message.'</strong>';
        }
        //Si le formulaire a deja ete envoye on recupere les donnes que lutilisateur avait deja insere
        if (isset($_POST['username'],$_POST['password'],$_POST['email'])) {
            $username = htmlentities($_POST['username'], ENT_QUOTES, 'UTF-8');
            if ($_POST['password']==$_POST['passverif']) {
                $password = htmlentities($_POST['password'], ENT_QUOTES, 'UTF-8');
            } else {
                $password = '';
            }
            $email = htmlentities($_POST['email'], ENT_QUOTES, 'UTF-8');
        //$avatar = htmlentities($_POST['avatar'], ENT_QUOTES, 'UTF-8');
        } else {
            //Sinon, on affiche les donnes a partir de la base de donnee
            $dnn = $bdd->query('SELECT username,password,email FROM users WHERE username="'.$_SESSION['username'].'"');
            $dn = $dnn->fetch();
            $username = htmlspecialchars($dn['username'], ENT_QUOTES, 'UTF-8');
            $password = htmlspecialchars($dn['password'], ENT_QUOTES, 'UTF-8');
            $email = htmlspecialchars($dn['email'], ENT_QUOTES, 'UTF-8');
            //$avatar = htmlspecialchars($dn['avatar'], ENT_QUOTES, 'UTF-8');
        }
        //On affiche le formulaire?>
<div class="content">
    <form action="edit_infos.php" method="post">
        <strong>Vous pouvez modifier vos informations:</strong><br />
        <div class="center">
            <label for="username">Nom d'utilisateur</label><input type="text" name="username" id="username" value="<?php echo $username; ?>" /><br />
            <label for="password">Mot de passe<span class="small">(6 caract&egrave;res min.)</span></label><input type="password" name="password" id="password" value="<?php echo $password; ?>" /><br />
            <label for="passverif">Mot de passe<span class="small">(v&eacute;rification)</span></label><input type="password" name="passverif" id="passverif" value="<?php echo $password; ?>" /><br />
            <label for="email">Email</label><input type="text" name="email" id="email" value="<?php echo $email; ?>" /><br />
            <!--<label for="avatar">Image perso<span class="small">(facultatif)</span></label><input type="text" name="avatar" id="avatar" value="<?php //echo $avatar;?>" /><br />-->
            <input type="submit" value="Envoyer" />
        </div>
    </form>
</div>
<?php
    }
} else {
    ?>
<div class="message">Pour acc&eacute;der &agrave; cette page, vous devez &ecirc;tre connect&eacute;.<br />
<a href="connexion.php">Se connecter</a></div>
<?php
}
?>

	</body>
</html>
