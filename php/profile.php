<?php
include('config.php');
///Page d edition de profils ///
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="../../css/main.css" rel="stylesheet" title="Style" />
        <title>Profil d'un utilisateur</title>
    </head>
    <body>
    	<div class="header">
        	<h1><span>E</span>space <span>M</span>embre</h1>
	    </div>
        <div class="content">
<?php
//On verifie que lidentifiant de lutilisateur est defini
if (isset($_GET['id'])) {
    $id = intval($_GET['id']);
    //On verifie que lutilisateur existe
    $dn = $bdd->query('select username, email, avatar, signup_date from users where id="'.$id.'"');
    if ($dn->rowCount()>0) {
        $dnn = $dn->fetch();
        //On affiche les donnees de lutilisateur?>
Voici le profil de "<?php echo htmlentities($dnn['username']); ?>" :
<table>
	<tr>
    	<td><?php
if ($dnn['avatar']!='') {
            echo '<img src="'.htmlentities($dnn['avatar'], ENT_QUOTES, 'UTF-8').'" alt="Image Perso" style="max-width:100px;max-height:100px;" />';
        } else {
            echo 'Cet utilisateur n\'a pas d\'image perso.';
        } ?></td>
    	<td class="left"><h1><?php echo htmlentities($dnn['username'], ENT_QUOTES, 'UTF-8'); ?></h1>
    	Email: <?php echo htmlentities($dnn['email'], ENT_QUOTES, 'UTF-8'); ?><br />
        Cet utilisateur s'est inscrit le <?php echo date('d/m/Y', $dnn['signup_date']); ?></td>
    </tr>
</table>
<?php
    } else {
        echo 'Cet utilisateur n\'existe pas.';
    }
} else {
    echo 'L\'identifiant de l\'utilisateur n\'est pas d&eacute;fini.';
}
?>
		<h3 class="connexition"><a href="users.php">Retour &agrave; la liste des utilisateurs</a></h3>
	</body>
</html>
