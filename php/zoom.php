      <?php
require 'recaptchalib.php';
?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<!-- Metas Page details-->
<title>Sky-&-Wave</title>
<meta name="description" content="sky&wind">
<meta name="author" content="Julioo">
<!-- Mobile Specific Metas-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--main style-->
<link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap-social-gh-pages/bootstrap-social.css">
  <link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap.css">
<link rel="stylesheet" type="text/css" media="screen" href="../css/main.css">
<link rel="stylesheet" type="text/css" media="screen" href="../css/responsive.css">
<link rel="stylesheet" href="../css/font-awesome.css">
<!--google font style-->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- pictos !-->
<link rel="stylesheet" href="css/font-awesome.css">
<script src="https://www.google.com/recaptcha/api.js"></script>
</head>
  <?php
$reCaptcha = new ReCaptcha("6LdrdEwUAAAAAMtaYy-WgLlwpE_29gyIj5_hwvt7");
if(isset($_POST["g-recaptcha-response"])) {
    $resp = $reCaptcha->verifyResponse(
        $_SERVER["REMOTE_ADDR"],
        $_POST["g-recaptcha-response"]
        );
    if ($resp != null && $resp->success) {echo "OK";}
    else {echo "CAPTCHA incorrect";}
    }
?> 
<!--head fin-->
<body class="backgroundHead" style="min-height:100vh;">
<?php include('session_user.php');?>
<div align="center">
<?php
// Récupération et affichage des offres !

$reponse = $bdd->query("
SELECT
  id_saisi_offre, message, prix, titre, localisation, lieu, Image,
  users.username
FROM
  saisi_offre AS SO
  INNER JOIN users ON SO.id_users = users.id
  WHERE id_saisi_offre = ".$_GET['id_saisi_offre']."
ORDER BY id_saisi_offre DESC LIMIT 0, 30");//retravailler sur la limite !

$donnees = $reponse->fetch();
    if (empty($donnees['Image'])) {
        $src = '/images/planche.svg';
    } else {
        $src = '/uploads/' . $donnees['Image'];
    }
    if (empty($donnees['titre'])) {
        $titre = '....';
    } else {
        $titre = $donnees['titre'];
    } ?>

<li class="<?php echo htmlspecialchars($donnees['localisation']);?> zoom">
  <div class="border-muted mb-3">
   <span class="text-primary"><strong><?php echo htmlspecialchars($donnees['username']); ?></span></br></strong>Annonce N° <?php echo $donnees['id_saisi_offre'];?><hr>
   <p><strong class="text-info">Titre : </strong><?php echo htmlspecialchars($titre); ?></p>
   <a class="zommImage" href="<?php echo htmlspecialchars($src); ?>"><img src="<?php echo htmlspecialchars($src); ?>"/></a>
   <hr>
   <p class="#"><strong class="text-info" >Lieu : </strong><?php echo htmlspecialchars($donnees['lieu']); ?></p>
<hr>
   <p class="messageZoom"><strong class="text-primary" >Description :</strong><?php echo htmlspecialchars($donnees['message']); ?></p>
<hr>
     <p><strong class="text-info">Prix :</strong><?php echo htmlspecialchars($donnees['prix']); ?></p>
<hr>
</div>
<?php
//si diferent de lutilisateur afficher le btn repondre
if (isset($_SESSION['username']) and $_SESSION['username'] != $donnees['username']) {
    ?>
<a style="padding-left:2vw;" onclick="window.location = '<?php echo '../php/mise_en_relation.php?id_saisi_offre='.htmlspecialchars($donnees['id_saisi_offre'])?>';" value="#" class="btn-social btn btn-success"><span class="fa fa-anchor"></span>Message privé !</a>
<?php
}
    //si il y a utilisateur
if (isset($_SESSION['username'])) {
    ?>
<hr>
<form action="../Mini-chat/cible_chat.php?id_saisi_offre=<?php echo htmlspecialchars($_GET['id_saisi_offre'])?>" method="post">


<div class="input-group">
  <div class="input-group-prepend">
    <span class="bg-info text-light input-group-text">Ou</br> Posé une Question: </span>
  </div>
  <textarea class="form-control" aria-label="With textarea" placeholder="taper un message (70 caractères max) !" type="text" name="message"  rows="4" cols="40"></textarea>
</div>


<hr>
  <div class="g-recaptcha" data-sitekey="6LdrdEwUAAAAAKbk58j7eCmlmEBtGgNu-ytDnyT6"></div>
 <input type="submit" class="btn btn-outline-info" value="Envoyer">
 <input type="button" onclick="window.location ='../php/menu.php';" value="retour menu!" class="btn btn-outline-dark">
</form>
<hr>
<?php
$reponse = dbconnect()->query('SELECT * FROM chat_table
WHERE id_offre = "'.$_GET['id_saisi_offre'].'"
ORDER BY ID DESC LIMIT 0,9
');

    while ($donnees = $reponse->fetch()) {
        echo '<div class="card border-muted mb-3" style="width: 38rem;">
    <div class="bg-info text-light card-header">' . htmlspecialchars(substr($donnees['pseudo'], 0, 30)) . '</div>
    <div class="card-body">
      <h4 class="card-text messageZoom">' . htmlspecialchars(substr($donnees['message'], 0, 70)).'</h4>
      <p class="text-info card-text">'. htmlspecialchars($donnees['date_creation']) .'</p>
      </br>
      <p class="mb-0"></p>
    </div></div>';
    }
    $reponse->closeCursor();
}
?>
</li>

</br>
	<input type="button" onclick="window.location = '../index.php';" value="Retour a Accueil" class="btn btn-warning">
</div>
</body>
</html>
