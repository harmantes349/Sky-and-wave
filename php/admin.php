<?php
//je recupere le givenname et je le transforme pour ma table username
require_once "config.php";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<!-- Metas Page details-->
<title>Sky-&-Wave</title>
<meta name="description" content="sky&wind">
<meta name="author" content="Julioo">
<!-- Mobile Specific Metas-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--main style-->
<link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap.css">
<link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap-social-gh-pages/bootstrap-social.css">
<link rel="stylesheet" type="text/css" media="screen" href="../css/main.css">
<link rel="stylesheet" type="text/css" media="screen" href="../css/responsive.css">
  <script type="text/javascript" src="../css/bootstrap.js"></script>
<!--google font style-->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- pictos !-->
<link rel="stylesheet" href="../css/font-awesome.css">
</head>
<body onUnload="GUnload()">
   <h2 class="text-primary">Espace Julioo</h3>
   <div id="preloader">
   	<div id="status"></div>
   </div>
  <ul class="border t_menu bg-light portfolioFilter" style="width: 100vw!important;">
 <li><a class="btn btn-outline-success" href="#" data-filter="*" class="current">Tout les utilisaateurs</a></li>
 <li><a class="btn btn-outline-primary" href="#" data-filter=".users">utilisateurs Actifs</a></li>
 <li><a class="btn btn-outline-danger" href="#" data-filter=".bann">utilisateur bans</a></li>
 </ul>

 <ul class="portfolioContainer isotope">
  <?php
  $reponse = $bdd->query('SELECT *
  FROM
    users
  ');
///////////////////
  while ($donnees = $reponse->fetch()) {
      if (isset($donnees['status']) and $donnees['status'] == '1') {
          echo '<li class="bann card isotope-item" style="width:18rem;height:auto!important;">';

          echo '<p class="text-danger card-text">' . htmlspecialchars($donnees['username']) . ' IS BAN</p>';

          echo '
<p class="text-dark card-text">' . htmlspecialchars($donnees['email']) . '</p>';

          echo '<form class="formGestion" action="cible_admin.php" method="post">
<input type="hidden" value="'.$donnees['id'].'" name="hidden">
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
          <label class="btn btn-outline-success">
            <input type="radio" name="reqSelec" value="unBann">UNBAN
          </label>
        </div>
          <input type="submit" value="OK" class="btn btn-info">
    </form>
</li>';
      } else {
          echo '<li class="users card isotope-item" style="width:18rem;height:auto!important;">';

          echo '<p class="text-primary card-text">' . htmlspecialchars($donnees['username']) . '</p>';

          echo '
<p class="text-dark card-text">' . htmlspecialchars($donnees['email']) . '</p>';

          echo '<form class="formGestion" action="cible_admin.php" method="post">
  <input type="hidden" value="'.$donnees['id'].'" name="hidden">
          <div class="btn-group btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-outline-danger">
              <input type="radio" name="reqSelec" value="Bann">Bann
            </label>
            <label class="btn btn-outline-warning">
              <input type="radio" name="reqSelec" value="suppPost">suppPost
            </label>
          </div>

            <input type="submit" value="OK" class="btn btn-info">
      </form>
  </li>';
      }
  }

  //requete delete users

  ?>
</ul>
    <input type="button" onclick="window.location = '../index.php';" value="Retour main" class="btn btn-secondary">
</body>

<!--jquary min js-->
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/bootstrap.js"></script>
<!--for portfolio jquery-->
<script src="../js/jquery.isotope.js" type="text/javascript"></script>
<!-- <link type="text/css" rel="stylesheet" id="theme" href="../css/jquery-ui-1.8.16.custom.css"> -->

<!--about jquery-->
<script src="../js/jquery.classyloader.min.js"></script>
<script src="../js/custom.js"></script>
</html>
