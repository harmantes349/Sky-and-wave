
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/main.css" rel="stylesheet" title="Style" />
        <link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap.css">
        <link href="../css/responsive.css" rel="stylesheet" title="Style" />
        <link href="../css/font-awesome.css" rel="stylesheet" title="Style" />
        <title>Liste des offres utilisateurs</title>
    </head>
    <body style="overflow-x:hidden; background: linear-gradient(to left, #fff, #17A2B8);" style="min-height:100vh;">

<?php include('session_user.php');?>

<div align="center">

<div id="div_alert" class="alert alert-light fontold" role="alert">
  <h3 class="fontSeoSc alert-heading border backgroundb text-primary">Voici la liste de vos Annonces</h3>
</div>
<div id="div_alert" class="alert alert-light text-primary border-primary" role="alert" >
<p>Ici vous pouvez les supprimés ou</br> Accéder aux commentaires !</p>
</div>
    <?php
    // Récupération et affichage des offres !
    $reponse = $bdd->query('
    SELECT
      id_saisi_offre, message, prix, titre, localisation, Image, lieu,
      users.username
    FROM
      saisi_offre AS SO
      INNER JOIN users ON SO.id_users = users.id
    ');

    while ($donnees = $reponse->fetch()) {
        if (empty($donnees['Image'])) {
            $src = '/images/planche.svg';
        } else {
            $src = '/uploads/' . $donnees['Image'];
        }
        if (empty($donnees['titre'])) {
            $titre = '....';
        } else {
            $titre = htmlspecialchars($donnees['titre']);
        }

        if (isset($_SESSION['username'])and $_SESSION['username'] == $donnees['username']) {
            ?>
            <!--Formulaire de suppression -->
           <form class="formGestion" action="cible_gerer_offres.php" method="post">
            <li style="max-width:80vw;" class="<?php echo htmlspecialchars($donnees['localisation']); ?> card border-primary" style="width: 28rem;">
            <?php echo '<a class="btn btn-primary" href="zoom.php?id_saisi_offre='.htmlspecialchars($donnees['id_saisi_offre']).'">Vers les commentaires </a>'; ?></br>
            <a align="center" href="<?php echo htmlspecialchars($src); ?>"><img class="card-img" alt="fORMAT NON SUPORTER !" class="imagePlanche" style="background:#fff;" src="<?php echo htmlspecialchars($src); ?>"/></a>
              <div class="card-body">
               <h5 align="center" class="card-title text-primary"<strong><?php echo htmlspecialchars($titre); ?></h5></strong>
               <strong class="text-primary"><p class="card-text">Auteur : </strong><?php echo htmlspecialchars($donnees['username']); ?></p>
                  <p  class="card-text messageUtil"><strong class="text-info">Description : <?php echo '<a class="#" href="zoom.php?id_saisi_offre='.htmlspecialchars($donnees['id_saisi_offre']).'">      <i class="text-info fa fa-search fa-2x"></i></a>'; ?></strong><?php echo htmlspecialchars($donnees['message']); ?></p>
                  <p><strong class="text-info card-text">Prix :</strong><span class=""><?php echo htmlspecialchars($donnees['prix']); ?></span></p>
                  <p><strong class="text-info card-text">Lieu :</strong><?php echo htmlspecialchars($donnees['lieu']); ?></p>

            <input type="hidden" value="<?php echo htmlspecialchars($donnees['id_saisi_offre']) ?>" name="hidden">
               <div class="btn-group btn-group-toggle" data-toggle="buttons">
                 <label class="btn btn-outline-danger">
                   <input type="radio" name="reqSelec" value="Supprimer ?"> Suppr ?
                 </label>
               </div>
                 <input type="submit" value="OK" class="btn btn-success">
           </form>
            </br>
<?php
        } ?>
    </li>
    <?php
    }



$reponse->closeCursor();
    ?>

			  <input type="button" onclick="window.location = 'menu.php';" value="Retour &agrave; l'Espace Membre" class="btn btn-secondary">
<hr>
</div>
</div>
  </body>
  <!--jquary min js-->
  <script type="text/javascript" src="../js/jquery.min.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <!--for portfolio jquery-->
  <script src="../js/jquery.isotope.js" type="text/javascript"></script>
  <!-- <link type="text/css" rel="stylesheet" id="theme" href="../css/jquery-ui-1.8.16.custom.css"> -->

  <!--about jquery-->
  <script src="../js/jquery.classyloader.min.js"></script>
  <script src="../js/custom.js"></script>
</html>
