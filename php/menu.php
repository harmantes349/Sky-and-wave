
<!DOCTYPE html PUBLIC>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <link href="../css/main.css" rel="stylesheet" title="Style" />
          <link href="../css/responsive.css" rel="stylesheet" title="Style" />
          <link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap.css">
          <link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap-social-gh-pages/bootstrap-social.css">
          <link rel="stylesheet" href="../css/font-awesome.css">
        <title>Espace membre</title>
    </head>
    <body style="background: linear-gradient(to left, #fff, #17A2B8);">

<?php include('session_user.php');?>
    	<div align="center">
        <div id="div_alert" class="alert alert-light fontold" role="alert">
          <h3 class="fontSeoSc alert-heading border backgroundb text-primary">Mon Espace</h3>

        </div>

        <ul>
          <li>
          <a onclick="window.location = 'requete.php';"  value="" class="btn btn-block btn-social btn-success"><span class="fa fa-anchor"></span>Gerer mes requetes</a>
        </li>
      </br>
        <li>
          <a onclick="window.location = 'gerer_offres.php';"  value="" class="btn btn-block btn-social btn-primary"><span class="fa fa-edit"></span>Commenté ou Supprimer mes Offres</a>
</li>
</br>
<li >
          <a onclick="window.location = 'edit_infos.php';"  value="" class="btn btn-block btn-social btn-info"><span class="fa fa-info"></span>Modifier mon Profil !</a>
</li>
</br>
<li >
          <a onclick="window.location = '../GoogleLogin/logout.php';"  value="" class="btn btn-block btn-social btn-danger"><span class="fa fa-sign-out"></span>Déconnecter</a>
</li>
</br>
<li >
          <a onclick="window.location = '../index.php';"  value="" class="btn btn-block btn-social btn-secondary"><span class="fa fa-arrow-circle-o-left"></span>Retour</a>
</li>
        </ul>
</div>

	</body>
  <!--jquary min js-->
  <script type="text/javascript" src="../js/jquery.min.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <!--for portfolio jquery-->
  <script src="../js/jquery.isotope.js" type="text/javascript"></script>
  <!-- <link type="text/css" rel="stylesheet" id="theme" href="../css/jquery-ui-1.8.16.custom.css"> -->

  <!--about jquery-->
  <script src="../js/jquery.classyloader.min.js"></script>
  <script src="../js/custom.js"></script>
</html>
