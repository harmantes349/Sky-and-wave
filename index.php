<?php
//je recupere le givenname et je le transforme pour ma table username
require_once "GoogleLogin/config.php";
  $loginURL = $gClient->createAuthUrl();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<!-- Metas Page details-->
<title>Sky-&-Wave</title>
<meta name="description" content="sky&wind">
<meta name="author" content="Julioo">
<!-- Mobile Specific Metas-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--main style-->
<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap-social-gh-pages/assets/css/bootstrap.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap-social-gh-pages/bootstrap-social.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/main.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/responsive.css">
  <script type="text/javascript" src="css/bootstrap.js"></script>
<!--google font style-->
<link href="https://fonts.googleapis.com/css?family=Sedgwick+Ave+Display" rel="stylesheet"> 
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- pictos !-->
<link rel="stylesheet" href="css/font-awesome.css">
</head>
<!--head fin
-->

<body onUnload="GUnload()">
  <div class="backgroundHead2">
  <div id="preloader">
  	<div id="status"></div>
  </div>
<?php include('php/session_user.php');?>
  <div class="red"></div>

  <div align="center">
<img alt="Sky-and-Wave" id="titre_main" src="images/logo_sky.svg">
<img alt="Sky-and-Wave" id="titre_2" src="images/titre_sky.svg">
  <h5 class="alert alert-info" style="font-size: inherit;width:500px; max-width: 80%;"; id="descrip_site" align="center">Vente de Materiels et de services Nautiques</h5>
</br>
<!-- l'alert -->
  <?php
  if (isset($_GET['message']) && $_GET['message'] == "une Seule Requête est possible") {
      ?>
      <!-- l anim de l'alert -->
      <script>
function display(f)
{
var targetElement;
targetElement = document.getElementById(f) ;
if (targetElement.style.display == "none")
{
targetElement.style.display = "" ;
} else {
document.getElementById("div_alert").style.transform = "scale(0)";
}}
</script>
<div id="moitie">
<div id="div_alert" class="alert alert-danger" role="alert">
<p class="alert-heading"><button type="button"  onclick="javascript:display('div_alert');" class="close" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>Oups ... Envoyer que Une Seule Requête par offre !</p>
</div>
</div>
  <?php
  }
  ///////////////////////////////////////////////////
  ?>
  <!-- l'alert 2 -->
    <?php
    if (isset($_GET['message']) && $_GET['message'] == "Annonce non conforme !") {
        ?>
        <!-- l anim de l'alert -->
        <script>
  function display(f)
  {
  var targetElement;
  targetElement = document.getElementById(f) ;
  if (targetElement.style.display == "none")
  {
  targetElement.style.display = "" ;
  } else {
  document.getElementById("div_alert").style.transform = "scale(0)";
  }}
  </script>
  <div id="moitie">
  <div id="div_alert" class="alert alert-danger" role="alert">
  <p class="alert-heading"><button type="button"  onclick="javascript:display('div_alert');" class="close" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>Vous avez déja Répondue à cette Annonce !</p>
  </div>
  </div>
    <?php
    }
    /////////////////////alert ban//////////////////////////////
    if (isset($_GET['message']) && $_GET['message'] == "tu es ban!") {
        ?>
        <!-- l anim de l'alert -->
        <script>
  function display(f)
  {
  var targetElement;
  targetElement = document.getElementById(f) ;
  if (targetElement.style.display == "none")
  {
  targetElement.style.display = "" ;
  } else {
  document.getElementById("div_alert").style.transform = "scale(0)";
  }}
  </script>
  <div id="moitie">
  <div id="div_alert" class="alert alert-danger" role="alert">
  <p class="alert-heading"><button type="button"  onclick="javascript:display('div_alert');" class="close" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>Votre Compte est bannie du Site</p>
  </div>
  </div>
    <?php
    }
    ///////////////////////////////////////////////////
    ?>
      <?php
    /////////////////////alert inscription//////////////////////////////
    if (isset($_GET['message']) && $_GET['message'] == "Success") {
        ?>
        <!-- l anim de l'alert -->
        <script>
  function display(f)
  {
  var targetElement;
  targetElement = document.getElementById(f) ;
  if (targetElement.style.display == "none")
  {
  targetElement.style.display = "" ;
  } else {
  document.getElementById("div_alert").style.transform = "scale(0)";
  }}
  </script>
  <div id="moitie">
  <div id="div_alert" class="alert alert-success" role="alert">
  <p class="alert-heading"><button type="button"  onclick="javascript:display('div_alert');" class="close" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>Votre Inscription est OK !</p>
  </div>
  </div>
    <?php
    }
    ///////////////////////////////////////////////////
    ?>
</div>

  <section>


    <?php include('navbar/navbar.php');?>
    </section>
       <ul style="background: linear-gradient(to top, #fff, #17A2B8);" class="portfolioContainer">
          <?php //include('php/config.php');?>
          <?php include('php/reponse.php');?>
        </ul>



    <?php include('navbar/footer.php');?>


    <!--jquary min js-->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <!--for portfolio jquery-->
    <script src="js/jquery.isotope.js" type="text/javascript"></script>
    <link type="text/css" rel="stylesheet" id="theme" href="css/jquery-ui-1.8.16.custom.css">
    <script type="text/javascript" src="js/jquery.ui.widget.min.js"></script>
    <!--about jquery-->
    <script src="js/jquery.classyloader.min.js"></script>
    <script src="js/custom.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
</body>
</html>
