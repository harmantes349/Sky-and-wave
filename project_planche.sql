-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 06 mars 2018 à 21:06
-- Version du serveur :  5.7.19
-- Version de PHP :  7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `project_planche`
--

-- --------------------------------------------------------

--
-- Structure de la table `chat_table`
--

DROP TABLE IF EXISTS `chat_table`;
CREATE TABLE IF NOT EXISTS `chat_table` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_offre` int(11) NOT NULL,
  `pseudo` text NOT NULL,
  `message` text NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=129 DEFAULT CHARSET=utf8 COMMENT='Nya !';

--
-- Déchargement des données de la table `chat_table`
--

INSERT INTO `chat_table` (`id`, `id_offre`, `pseudo`, `message`, `date_creation`) VALUES
(101, 115, 'Bigben', 'qsdsdqsd', '2018-03-02 23:33:06'),
(96, 115, 'Bigben', 'sdqsdqs', '2018-03-02 23:27:51'),
(97, 115, 'Bigben', 'dsqdsqdqsd', '2018-03-02 23:27:55'),
(98, 115, 'Bigben', 'sqddqsdqsd', '2018-03-02 23:29:12'),
(99, 115, 'Bigben', 'dffdsfsdf', '2018-03-02 23:32:37'),
(100, 115, 'Bigben', 'a 32', '2018-03-02 23:32:44'),
(93, 108, 'Bigben', 'bienvenue sur mon annonce !', '2018-03-02 21:45:47'),
(94, 108, 'Jules', 'trop cool vegeta\r\n', '2018-03-02 22:01:33'),
(95, 108, 'Jules', 'bon je fais quoi', '2018-03-02 22:23:22'),
(102, 115, 'Bigben', 'qdqsdsqdsqd', '2018-03-02 23:33:11'),
(103, 115, 'Bigben', 'le 7 eme', '2018-03-02 23:33:19'),
(104, 108, 'Bigben', 'slt le man', '2018-03-02 23:34:16'),
(105, 117, 'Bigben', 'aller les gas', '2018-03-02 23:36:00'),
(106, 108, 'Bigben', 'bien trankill', '2018-03-02 23:43:42'),
(107, 108, 'Jules', 'le 6 eme', '2018-03-03 00:06:17'),
(108, 117, 'Jules', 'je kiff ce Mec !', '2018-03-03 00:08:38'),
(109, 119, 'Jules', 'laisse rentré le chat !', '2018-03-03 00:20:36'),
(110, 119, 'Jules', 'chattttttttttttttttttt', '2018-03-03 00:20:48'),
(111, 119, 'Jules', 'et pourquoi ?', '2018-03-03 00:21:00'),
(112, 115, 'Jules', 'c moi', '2018-03-03 00:27:09'),
(113, 120, 'julioo', 'C\'est le julioo 348', '2018-03-03 00:40:06'),
(114, 115, 'julioo', 'te couille', '2018-03-03 00:56:28'),
(115, 120, 'Jules', 'moi le 352', '2018-03-03 01:14:12'),
(116, 115, 'harmantes34', 'aaaaaaaaaaaaaaaaaaaaaaaaaa', '2018-03-03 04:57:46'),
(117, 126, 'harmantes34', 'zzzzzzzzzzzzzzzzzzzzzzzzz', '2018-03-03 04:59:28'),
(118, 120, 'harmantes34', 'lotro', '2018-03-03 05:00:43'),
(119, 117, 'Jules', 'gg bigben', '2018-03-03 16:15:59'),
(120, 117, 'Bigben', 'Merci les gas !', '2018-03-03 16:43:37'),
(121, 126, 'Bigben', 'lol', '2018-03-03 17:39:08'),
(122, 120, 'Jules', 'sdqddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd', '2018-03-04 04:45:19'),
(123, 118, 'Jules', 'yop !', '2018-03-04 05:34:08'),
(124, 115, 'Jules', 'yop yop', '2018-03-04 05:36:23'),
(125, 120, 'Bigben', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '2018-03-04 06:03:15'),
(126, 115, 'Bigben', 'ici', '2018-03-05 22:58:43'),
(127, 115, 'Bigben', 'je suis la', '2018-03-05 23:26:13'),
(128, 130, 'Bigben', 'yop\r\n', '2018-03-06 01:16:34');

-- --------------------------------------------------------

--
-- Structure de la table `mise_en_relation`
--

DROP TABLE IF EXISTS `mise_en_relation`;
CREATE TABLE IF NOT EXISTS `mise_en_relation` (
  `idreq` int(255) NOT NULL AUTO_INCREMENT,
  `id_demandeur` int(255) NOT NULL,
  `id_receveur` int(255) NOT NULL,
  `id_saisi_offre` int(255) NOT NULL,
  `date_creation` datetime DEFAULT CURRENT_TIMESTAMP,
  `date_acceptation` date DEFAULT NULL,
  `message_requete` varchar(255) NOT NULL,
  PRIMARY KEY (`idreq`)
) ENGINE=MyISAM AUTO_INCREMENT=162 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `mise_en_relation`
--

INSERT INTO `mise_en_relation` (`idreq`, `id_demandeur`, `id_receveur`, `id_saisi_offre`, `date_creation`, `date_acceptation`, `message_requete`) VALUES
(140, 49, 51, 120, '2018-03-03 05:04:42', NULL, 'ENORME §');

-- --------------------------------------------------------

--
-- Structure de la table `saisi_offre`
--

DROP TABLE IF EXISTS `saisi_offre`;
CREATE TABLE IF NOT EXISTS `saisi_offre` (
  `id_saisi_offre` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `titre` text NOT NULL,
  `message` text NOT NULL,
  `prix` varchar(21000) NOT NULL,
  `localisation` varchar(250) NOT NULL,
  `lieu` text NOT NULL,
  `Image` varchar(250) CHARACTER SET utf8 COLLATE utf8_latvian_ci NOT NULL,
  PRIMARY KEY (`id_saisi_offre`),
  KEY `id_users` (`id_users`)
) ENGINE=MyISAM AUTO_INCREMENT=141 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `saisi_offre`
--

INSERT INTO `saisi_offre` (`id_saisi_offre`, `id_users`, `titre`, `message`, `prix`, `localisation`, `lieu`, `Image`) VALUES
(138, 25, 'sqdsqd', 'sqdqdqd', 'dqsqd', 'Sud', 'qsddqsd', ''),
(132, 49, 'lollollollollollollolloll', 'lollollollollollollollollollollollollollollollollollollollollollollollollollollollollollollollollollollollollollol', 'lollollollollol', 'Sud', 'lollollollollollollolloll', ''),
(120, 51, 'slt c Julioo', 'sdsqdsqd', 'qsdsqdsqdsqdqsd', 'Nord', 'qsdqsdsqqsdqsdq', '11222622_1025969920769922_8738279471661309203_n.jpg'),
(140, 25, 'sdqqqqqqqqqqqqqqqqqqqqqqq', 'sdqqqqqqqqqqqqqqqqqqqqqqqqqqsdqqqqqqqqqqqqqqqqqqqqqqqqqqsdqqqqqqqqqqqqqqqqqqqqqqqqqqsdqqqqqqqqqqqqqqqqqqqqqqqqqqsdqqqqqqqqqqqqqqqqqqqqqqqqqqsdqqqqqqqqqqqqqqqqqqqqqqqqqqsdqqqqqqqqqqqqqqqqqqqqqqqqqqsdqqqqqqqqqqqqqqqqqqqqqqqqqqsdqqqqqqqqqqqqqqqqqqqqqqqqqqsdq', 'sdqqqqqqqqqqqqq', 'Sud', 'sdqqqqqqqqqqqqqqqqqqqqqqq', '');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(255) NOT NULL,
  `telephone` int(10) NOT NULL DEFAULT '0',
  `signup_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `telephone`, `signup_date`, `status`) VALUES
(24, 'harmantes347', 'harmantes347@gmail.com', 'ada8d6d9ee0fba07a8dcd2b94a88ae4f9a580a3a38da820137a590c701c41d47', 608070706, '2018-02-22 12:36:51', 0),
(35, 'Bigben', 'harmantes349@gmail.com', 'bbdefa2950f49882f295b1285d4fa9dec45fc4144bfb07ee6acc68762d12c2e3', 0, '2018-02-22 20:14:42', 0),
(25, 'harmantes34', 'harmantes@hotmail.fr', 'ada8d6d9ee0fba07a8dcd2b94a88ae4f9a580a3a38da820137a590c701c41d47', 1234567891, '2018-02-22 13:17:04', 0),
(36, 'harmantes348', 'harmantes348@gmail.com', 'ada8d6d9ee0fba07a8dcd2b94a88ae4f9a580a3a38da820137a590c701c41d47', 0, '2018-02-22 22:09:12', 1),
(49, 'Jules', 'harmantes352@gmail.com', 'bbdefa2950f49882f295b1285d4fa9dec45fc4144bfb07ee6acc68762d12c2e3', 0, '2018-03-01 15:06:06', 0),
(51, 'julioo', 'harmantes348@gmail.com', 'bbdefa2950f49882f295b1285d4fa9dec45fc4144bfb07ee6acc68762d12c2e3', 0, '2018-03-01 15:43:37', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
